**_Simulation "Epidémie"_**

Simulation basique avec des équations différentielles, sans classe. Ouvrir ce fichier .mo et lancer la simulation.


**_Simulation "VariantZeta"_**

Pour lancer cette simulation:


1. Télécharger le dossier VariantZeta.rar et le décompresser.

2. Ouvrir le dossier dans Modelica.

3. Dans le modèle _test_, faites glisser les modèles de _Components_ de votre choix pour créer votre assemblage.

> Remarque: un assemblage vous a été laissé dans _test_. Il représente l'évolution d'une épidémie entre sains, infectés et rétablis. Les sains se reproduisent entre eux et les infectés peuvent décéder. Vous pouvez supprimer cet assemblage pour créer le votre.

Utilisation des modèles de _Components_:
> Remarque: les noms des composants sont tirés du modèle Lotka-Volterra du site _ModelicaByExample_: https://mbe.modelica.university/components/components/population/ . Ils ne sont pas forcément très évocateurs, mais ils ne pouvaient pas être modifiés a posteriori. 
- RegionalPopulation: ce bloc représente une population donnée A. L'effectif A de cette population est suivi dans le tracé lorsque vous simulez votre assemblage. Double-cliquez dessus pour paramétrer la population initiale A0. La connexion se trouve en haut du bloc.

- Predation: ce bloc déclenche l'effet de "prédation" d'une population B sur une population A. L'effet est de la forme der(A)=-beta.A.B et der(B)=beta.A.B . Double-cliquez sur le bloc pour paramétrer beta. La connexion vers la population A se trouve à gauche du bloc, et la connexion vers la population B se trouve à droite du bloc.

- InputFlow: ce bloc déclenche l'influence d'une population B sur une population A. L'effet est de la forme der(A)=-gamma.B et der(B)=gamma.B . Double-cliquez sur le bloc pour paramétrer gamma. La connexion vers la population A se trouve à gauche du bloc, et la connexion vers la population B se trouve à droite du bloc.

- Reproduction: ce bloc déclenche l'augmentation d'une population A sans transfert vers une autre population. L'effet est de la forme der(A)=alpha.A . Double-cliquez sur le bloc pour paramétrer alpha. La connexion vers la population A se trouve en haut du bloc.

- Starvation: ce bloc déclenche la diminution d'une population A sans transfert vers une autre population. L'effet est de la forme der(A)=-gamma.A . Double-cliquez sur le bloc pour paramétrer gamma. La connexion vers la population A se trouve en haut du bloc.

4. Relier les composants entre eux.

5. Lancer la simulation de votre assemblage.
