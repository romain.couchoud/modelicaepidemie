model VariantAlpha
  parameter Real beta = 1e-3;
  parameter Real gamma = 1e-1;
  parameter Real S0 = 10000;   
  parameter Real I0 = 1;
  parameter Real R0 = 0;
  Real S;
  Real I;
  Real R;
  
initial equation
  S=S0;
  I=I0;
  R=R0;  
   
equation
  der(S)=-beta*S*I;
  der(I)=beta*S*I-gamma*I;
  der(R)=gamma*I;
  
end VariantAlpha;
